package com.github.xuejike.query.core.base;

import cn.hutool.core.collection.ListUtil;
import com.github.xuejike.query.core.criteria.DaoCriteria;
import com.github.xuejike.query.core.criteria.LoadRefCriteria;
import com.github.xuejike.query.core.criteria.MapCriteria;
import com.github.xuejike.query.core.criteria.SelectDaoCriteria;
import com.github.xuejike.query.core.enums.LoadRefMode;
import com.github.xuejike.query.core.po.FieldInfo;
import com.github.xuejike.query.core.po.LoadRefInfo;
import com.github.xuejike.query.core.tool.lambda.FieldFunction;
import com.github.xuejike.query.core.tool.lambda.LambdaTool;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author xuejike
 * @date 2020/12/18
 */
public class BaseNestedWhereQuery<T,F,C extends BaseWhereQuery<T,F,C>>  extends BaseSimpleWhereQuery<T,F,C> implements LoadRefCriteria<F,C>{

    public BaseNestedWhereQuery() {

    }

    public BaseNestedWhereQuery(C returnObj) {
        super(returnObj);
    }

    public<R extends BaseNestedWhereQuery<T,F,R> > C or(Consumer<BaseNestedWhereQuery<T,F,R>> or){
        BaseNestedWhereQuery<T,F,R> query = new BaseNestedWhereQuery<T,F,R>();
        or.accept(query);
         if (query.isNotEmpty()){
             orList.add(query);
         }
         return returnObj;
     }

    @Override
    public <X> C loadRef(F refField, Class<X> entityCls, FieldFunction<X, ?> targetField, LoadRefMode mode,FieldFunction<X,?> ... selectedFields) {
        LoadRefInfo<X> info = new LoadRefInfo<>();
        info.setRefClass(entityCls);
        info.setMode(mode);
        info.setTargetField(buildFieldInfo(targetField));
        List<FieldInfo> selectedList = Optional.ofNullable(selectedFields)
                .map(it -> Arrays.stream(it).map(this::buildFieldInfo).collect(Collectors.toList())).orElse(ListUtil.empty());
        selectedList.add(info.getTargetField());
        info.setSelectedFieldList(selectedList);
        refClassMap.put(buildFieldInfo(refField),info);
        return returnObj;
    }


}

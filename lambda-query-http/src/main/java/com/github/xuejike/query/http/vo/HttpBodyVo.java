package com.github.xuejike.query.http.vo;

import com.github.xuejike.query.core.enums.OrderType;
import com.github.xuejike.query.core.enums.WhereOperation;
import com.github.xuejike.query.core.po.BaseConditionsVo;
import com.github.xuejike.query.core.po.FieldInfo;
import com.github.xuejike.query.core.po.LoadRefInfo;
import com.github.xuejike.query.core.po.QueryInfo;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author xuejike
 * @date 2020/12/31
 */
@Data
public class HttpBodyVo extends BaseConditionsVo {


}
